<?php

$plugin = array(
  'title' => t('Fontslider'),
  'single' => TRUE,
  'category' => array(t('TFT'), -9),
  'defaults' => array('admin_title' => 'jQuery Font Slider'),
  'edit form' => 'jquery_fontslider_content_type_edit_form',
  'add form' => 'jquery_fontslider_content_type_edit_form',
  'edit text' => t('Edit'),
  'render callback' => 'jquery_fontslider_content_type_render',
  'all contexts' => TRUE,
);

function jquery_fontslider_content_type_render($subtype, $conf, $args, $context = NULL) {
  drupal_add_library('system', 'ui.slider');
  drupal_add_css(drupal_get_path('module', 'jquery_fontslider') . '/css/jquery_fontslider.css');
  drupal_add_js(drupal_get_path('module', 'jquery_fontslider') . '/js/jquery_fontslider.js');
  $block = new stdClass;
  $block->title = '';
  $block->content = '<div id="fontslider-wrapper"><div id="fontslider-label">Font:</div><div id="fontslider">&nbsp;</div></div>';
  return $block;
}

/**
 * Returns an edit form for the custom type.
 */
function jquery_fontslider_content_type_edit_form($form, &$form_state) {
  $settings = $form_state['conf'];
  $form_state['settings'] = $settings;

  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($settings['admin_title']) ? $settings['admin_title'] : '',
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this pane. If blank, the regular title will be used.'),
  );
  return $form;
}
