(function($){
$(document).ready(function(){
  jQuery("#fontslider").slider({ 
    min: 10,
  max: 20,
  step: 1,
  value: 14,
  slide: function(event, ui) {
    $('.node .content').css("font-size", ui.value + 'px');
    $('.node .content').css("line-height", (ui.value+5) + 'px');
    $('.pane-node-body').css("font-size", ui.value + 'px');
    $('.pane-node-body').css("line-height", (ui.value+5) + 'px');
  }
  });
});
})(jQuery);
